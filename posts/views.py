from django.shortcuts import render
from django.http import HttpResponse
from .models import Post
from leiras.models import Leiras 
from hatter.models import Hatter 

# Create your views here.

def napelem_home(request):
    queryset = Post.objects.all()
    content = Leiras.objects.all()
    backgrounds = Hatter.objects.all()
    context = {
		"object_list": queryset,
        "type": 'Napelemes erőmű',
        "content": content,
        "backgrounds": backgrounds
	}
    return render(request, "base.html", context)

def intelligens_home(request):
    queryset = Post.objects.all()
    content = Leiras.objects.all()
    backgrounds = Hatter.objects.all()
    context = {
		"object_list": queryset, 
        "type": 'Intelligens épület',
        "content": content,
        "backgrounds": backgrounds
	}
    return render(request, "base.html", context)

def rb_home(request):
    queryset = Post.objects.all()
    content = Leiras.objects.all()
    backgrounds = Hatter.objects.all()
    context = {
		"object_list": queryset, 
        "type": 'RB technika',
        "content": content,
        "backgrounds": backgrounds 
	}
    return render(request, "base.html", context)

def kivitelezes_home(request):
    queryset = Post.objects.all()
    content = Leiras.objects.all()
    backgrounds = Hatter.objects.all()
    context = {
		"object_list": queryset, 
        "type": 'Generál kivitelezés',
        "content": content,
        "backgrounds": backgrounds
	}
    return render(request, "base.html", context)

def home(request):
    backgrounds = Hatter.objects.all()
    context = {
        "backgrounds": backgrounds
	}
    return render(request, "index.html", context)

def mobile_home(request):
    backgrounds = Hatter.objects.all()
    context = {
        "backgrounds": backgrounds
	}
    return render(request, "mobile.html", context)