from django.contrib import admin
from .models import Post
# Register your models here.
class PostModelAdmin(admin.ModelAdmin):
	list_display = ["cim", "letrehozas","kategoria"]
	list_display_links = ["cim"]
	list_filter = [ "letrehozas", "kategoria"]
	search_fields = ["cim", "tartalom", "kategoria"]
	class Meta:
		model = Post


admin.site.register(Post, PostModelAdmin)