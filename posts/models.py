from django.db import models

# Create your models here.
class Post(models.Model):
    cim = models.CharField(max_length=120)
    kep = models.ImageField(
            null=True, 
            blank=True, )
    tartalom = models.TextField()
    letrehozas = models.DateTimeField(auto_now=False, auto_now_add=True)
    kategoria = models.CharField(max_length=256,default='Napelemes erőmű' ,choices=[('Napelemes erőmű','Napelemes erőmű'),( 'Intelligens épület','Intelligens épület'),( 'Generál kivitelezés','Generál kivitelezés'),( 'RB technika','RB technika')])
    def __str__(self):
        return self.cim
