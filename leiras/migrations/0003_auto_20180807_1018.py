# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2018-08-07 08:18
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('leiras', '0002_auto_20180807_1018'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='leiras',
            options={'verbose_name_plural': 'Leírások'},
        ),
    ]
