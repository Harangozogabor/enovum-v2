from django.contrib import admin
from .models import Leiras
# Register your models here.
class LeirasModelAdmin(admin.ModelAdmin):
	list_display = ["tartalom","kategoria"]
	list_display_links = ["tartalom"]
	class Meta:
		model = Leiras


admin.site.register(Leiras, LeirasModelAdmin)