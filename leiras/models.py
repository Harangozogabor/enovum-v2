from django.db import models

# Create your models here.
class Leiras(models.Model):
    tartalom = models.TextField()
    objects = models.Manager()
    kategoria = models.CharField(max_length=256,default='Napelemes erőmű' ,choices=[('Napelemes erőmű','Napelemes erőmű'),( 'Intelligens épület','Intelligens épület'),( 'Generál kivitelezés','Generál kivitelezés'),( 'RB technika','RB technika')])
    def __str__(self):
        return self.tartalom
    class Meta:
        verbose_name_plural = "Leírások"