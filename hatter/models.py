from django.db import models

# Create your models here.
class Hatter(models.Model):
    cim = models.TextField(null=True)
    kep = models.ImageField(
            null=True, 
            blank=True, )
    letrehozas = models.DateTimeField(auto_now=False, auto_now_add=True)
    def __str__(self):
        return self.cim
    class Meta:
        verbose_name_plural = "Hátterek"